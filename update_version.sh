
#!/bin/bash

# Get the last commit date (format: yymmdd)
LAST_COMMIT_DATE=$(git log -1 --format=%cd --date=format:%y%m%d)

# Get the first eight digits of the commit hash
COMMIT_HASH=$(git log -1 --format=%h)


UpdatedVersion="${LAST_COMMIT_DATE}-${COMMIT_HASH}"

mv target/assignment-0.0.1-SNAPSHOT.jar target/$UpdatedVersion.jar

echo "$UpdatedVersion"

